package com.lduparc.material.asymetric.widget;

import android.view.View;

public interface PoolObjectFactory<T extends View> {
    public T createObject();
}
