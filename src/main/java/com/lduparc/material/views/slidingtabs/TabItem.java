package com.lduparc.material.views.slidingtabs;

import android.support.v4.app.Fragment;

public abstract class TabItem {
    protected final CharSequence mTitle;
    protected final int mIndicatorColor;
    protected final int mDividerColor;
    protected final int[] mSelectorTextColor;

    public TabItem(CharSequence title, int indicatorColor, int dividerColor, int[] selectorTextColor) {
        mTitle = title;
        mIndicatorColor = indicatorColor;
        mDividerColor = dividerColor;
        mSelectorTextColor = selectorTextColor;
    }

    public abstract Fragment createFragment();

    public CharSequence getTitle() {
        return mTitle;
    }

    public int getIndicatorColor() {
        return mIndicatorColor;
    }

    public int getDividerColor() {
        return mDividerColor;
    }

    public int[] getSelectorTextColor() {
        return mSelectorTextColor;
    }

}
