package com.lduparc.material.widgets;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.InputFilter;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lduparc.material.R;
import com.lduparc.material.views.ButtonFlat;

@SuppressWarnings("all")
public class Dialog extends android.app.Dialog {

    public Dialog(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.dialog);
    }

    public Dialog(Context context, boolean cancelable) {
        super(context);
        setCancelable(cancelable);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.dialog);
    }

    public Dialog(Context context, boolean cancelable, boolean edittable) {
        super(context);
        setCancelable(cancelable);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(edittable ? R.layout.edittable_dialog : R.layout.dialog);
    }

    public void setBackground(int resource) {
        RelativeLayout back = (RelativeLayout) findViewById(R.id.background);
        back.setBackgroundResource(resource);
    }

    public void setBackground(Drawable image) {
        RelativeLayout back = (RelativeLayout) findViewById(R.id.background);
        if (Build.VERSION.SDK_INT > 15)
            back.setBackground(image);
        else back.setBackgroundDrawable(image);
    }

    public void setBackgroundColor(int color) {
        RelativeLayout back = (RelativeLayout) findViewById(R.id.background);
        back.setBackgroundColor(color);
    }

    public void setTitle(String title) {
        TextView tv = (TextView) findViewById(android.R.id.text1);
        tv.setText(title);
    }

    /*
    Setting icon... not in actual android l dialogs but in case you wanna use it..
    */
    public void setIcon(int Resource) {
        ImageView img = (ImageView) findViewById(R.id.image);
        img.setBackgroundResource(Resource);
    }

    public void setIcon(Drawable icon) {
        ImageView img = (ImageView) findViewById(R.id.image);
        img.setImageDrawable(icon);
    }

    public void setTitleColor(int Color) {
        TextView tv = (TextView) findViewById(android.R.id.text1);
        tv.setTextColor(Color);
    }

    public void setMessageColor(int Color) {
        TextView tv = (TextView) findViewById(android.R.id.text2);
        tv.setTextColor(Color);
    }

    public String getTitle() {
        TextView tv = (TextView) findViewById(android.R.id.text1);
        return tv.getText().toString();
    }

    public String getMessage() {
        TextView tv = (TextView) findViewById(android.R.id.text2);
        return tv.getText().toString();
    }

    public void setTitleTextSize(int size) {
        TextView tv = (TextView) findViewById(android.R.id.text1);
        tv.setTextSize(size);
    }

    public void setMessageTextSize(int size) {
        TextView tv = (TextView) findViewById(android.R.id.text2);
        tv.setTextSize(size);
    }

    public void setMessage(String message) {
        TextView tv = (TextView) findViewById(android.R.id.text2);
        tv.setText(message);
    }

    public void setLimit(int limit) {
        if (limit > -1) {
            TextView tv = (TextView) findViewById(android.R.id.text2);
            InputFilter[] filterArray = new InputFilter[1];
            filterArray[0] = new InputFilter.LengthFilter(limit);
            tv.setFilters(filterArray);
        }
    }

    public void setPositiveButton(String text, int color, View.OnClickListener onClickListener) {
        ButtonFlat tv = (ButtonFlat) findViewById(android.R.id.button1);
        tv.setText(text);
        tv.setTextColor(color);
        tv.setOnClickListener(onClickListener);
    }

    public void setPositiveButtonVisibility(int visibility) {
        ButtonFlat tv = (ButtonFlat) findViewById(android.R.id.button1);
        if (tv != null)
            tv.setVisibility(visibility);
    }

    public void setNegativeButtonVisibility(int visibility) {
        ButtonFlat tv = (ButtonFlat) findViewById(android.R.id.button2);
        if (tv != null)
            tv.setVisibility(visibility);
    }

    public void setNegativeButton(String text, int color, View.OnClickListener onClickListener) {
        ButtonFlat tv = (ButtonFlat) findViewById(android.R.id.button2);
        tv.setText(text);
        tv.setTextColor(color);
        tv.setOnClickListener(onClickListener);
    }

    public void setPositiveButton(String text, View.OnClickListener onClickListener) {
        setPositiveButton(text, Color.DKGRAY, onClickListener);
    }

    public void setNegativeButton(String text, View.OnClickListener onClickListener) {
        setNegativeButton(text, Color.DKGRAY, onClickListener);
    }

    static public class Builder {
        Dialog dialog;

        public Builder(Context context, boolean cancelable) {
            this.context = context;
            dialog = new Dialog(this.context, cancelable);
        }

        public Builder(Context context, boolean cancelable, boolean edittable) {
            this.context = context;
            dialog = new Dialog(this.context, cancelable, edittable);
        }

        public Builder(Context context) {
            this.context = context;
            dialog = new Dialog(this.context);
        }

        private String title, message, buttonText1, buttonText2;
        private Drawable icon;
        private int negativeButtonVisibility = View.VISIBLE;
        private int positiveButtonVisibility = View.VISIBLE;
        private int logo = 0;
        private int limit = -1;
        private int positionColor = Color.DKGRAY;
        private int negativeColor = Color.DKGRAY;
        private View.OnClickListener onClickListener1, onClickListener2;
        private Context context;

        public Builder Title(String Title) {
            this.title = Title;
            return this;
        }

        public Builder Message(String Message) {
            this.message = Message;
            return this;
        }

        public Builder setPositiveButton(String buttonText, View.OnClickListener onClickListener) {
            this.buttonText1 = buttonText;
            this.onClickListener1 = onClickListener;
            return this;
        }

        public Builder setPositiveButton(String buttonText, int color, View.OnClickListener onClickListener) {
            this.buttonText1 = buttonText;
            this.positionColor = color;
            this.onClickListener1 = onClickListener;
            return this;
        }

        public Builder setPositiveButtonVisibility(int visibility) {
            this.positiveButtonVisibility = visibility;
            return this;
        }

        public Builder setNegativeButtonVisibility(int visibility) {
            this.negativeButtonVisibility = visibility;
            return this;
        }

        public Builder setTextLimit(int limit) {
            this.limit = limit;
            return this;
        }

        public Builder setNegativeButton(String buttonText, View.OnClickListener onClickListener) {
            this.buttonText2 = buttonText;
            this.onClickListener2 = onClickListener;
            return this;
        }

        public Builder setNegativeButton(String buttonText, int color, View.OnClickListener onClickListener) {
            this.buttonText2 = buttonText;
            this.negativeColor = color;
            this.onClickListener2 = onClickListener;
            return this;
        }

        public Builder setCancelable(boolean cancelable) {
            this.dialog.setCancelable(cancelable);
            return this;
        }

        /*
        Setting icon... not in actual android l dialogs but in case you wanna use it..
        */
        public Builder setIcon(int Resource) {
            this.logo = Resource;
            return this;
        }

        public Builder setIcon(Drawable drawable) {
            this.icon = drawable;
            return this;
        }

        public Dialog prepare() {
            dialog.setTitle(this.title);
            dialog.setMessage(this.message);
            dialog.setLimit(this.limit);
            dialog.setPositiveButtonVisibility(this.positiveButtonVisibility);
            dialog.setNegativeButtonVisibility(this.negativeButtonVisibility);
            dialog.setPositiveButton(this.buttonText1, this.positionColor, onClickListener1);
            dialog.setNegativeButton(this.buttonText2, this.negativeColor, onClickListener2);
            /*
            Setting icon... not in actual android l dialogs but in case you wanna use it..
            */
            if (logo != 0) {
                dialog.setIcon(logo);
            } else {
                dialog.setIcon(icon);
            }
            return dialog;
        }

        public Dialog show() {
            dialog = prepare();
            dialog.show();
            return dialog;
        }

        public Dialog dismiss() {
            dialog.dismiss();
            return dialog;
        }
    }
}